package com.example.demo.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class VersionController {

    @Value("\${service.version}")
    private val version: String? = null

    @GetMapping("/version")
    fun getVersion(): String? {
        return version
    }

}